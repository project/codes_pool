<?php

namespace Drupal\Tests\codes_pool\Kernel\Entity;

use Drupal\codes_pool\Entity\CodeCollection;
use Drupal\codes_pool\Entity\CodeCollectionType;
use Drupal\file\Entity\File;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the code collection entity.
 *
 * @coversDefaultClass \Drupal\codes_pool\Entity\CodeCollection
 *
 * @group codes_pool
 */
class CodeCollectionTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'codes_pool',
    'entity',
    'file',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('codes_pool_collection');
    $this->installEntitySchema('file');
    $this->installConfig(['codes_pool', 'file']);
  }

  /**
   * @covers ::getName
   * @covers ::setName
   * @covers ::getDescription
   * @covers ::setDescription
   * @covers ::setSource
   * @covers ::getSourcePlugin
   * @covers ::getWeight
   * @covers ::setWeight
   * @covers ::getCodesCount
   * @covers ::getCountCodeAvailable
   * @covers ::hasCodeAvailable
   * @covers ::isEnabled
   * @covers ::setEnabled
   */
  public function testCodeCollection() {

    $code_collection_type = CodeCollectionType::create([
      'id' => 'test',
      'label' => 'Test label',
      'description' => 'Test description',
    ]);
    $code_collection_type->save();

    $csv_file = File::create([
      'uri' => \Drupal::service('file_system')->realpath(__DIR__ . '/../../../fixtures/codes.csv'),
      'filename' => 'codes.csv',
    ]);
    $csv_file->save();

    // @todo Actually set a bundle.
    $code_collection = CodeCollection::create([
      'status' => TRUE,
      'bundle' => 'test',
      'source' => [
        'target_plugin_id' => 'csv',
        'target_plugin_configuration' => [
          'file_upload' => [$csv_file->id()],
        ],
      ],
    ]);
    $code_collection->save();

    $code_collection->setName('March code collection');
    $this->assertEquals('March code collection', $code_collection->getName());

    $code_collection->setDescription('March 20% off code collection.');
    $this->assertEquals('March 20% off code collection.', $code_collection->getDescription());

    $code_collection->setEnabled(TRUE);
    $this->assertEquals(TRUE, $code_collection->isEnabled());
  }

}
