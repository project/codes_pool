<?php

namespace Drupal\Tests\codes_pool\Kernel\Plugin\codes_pool\CollectionSource;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the "CSV" source.
 *
 * @coversDefaultClass \Drupal\codes_pool\Plugin\codes_pool\CollectionSource\Csv
 *
 * @group codes_pool
 */
class CsvTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'codes_pool',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('codes_pool_collection');
    $this->installConfig(['codes_pool']);
  }

  /**
   * Tests the import.
   *
   * @covers ::import
   */
  public function testImport() {
    $this->assertTrue(TRUE);
  }

}
