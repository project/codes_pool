<?php

namespace Drupal\Tests\codes_pool\Kernel\Plugin\codes_pool;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the "codes_pool_testing_local_source" source.
 *
 * @group codes_pool
 */
class LocaleStorageTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'codes_pool',
    'codes_pool_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('codes_pool_collection');
    $this->installConfig(['codes_pool']);
  }

  /**
   * Tests the code generation.
   */
  public function testCodeGeneration() {
    /** @var \Drupal\codes_pool\CodeCollectionSourceManager $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.codes_pool_collection_source');
    /** @var \Drupal\codes_pool_test\Plugin\codes_pool\CollectionSource\LocalSource $plugin */
    $plugin = $plugin_manager->createInstance('codes_pool_testing_local_source');
    $codes = $plugin->dispenseCodes(['quantity' => 3]);
    $this->assertCount(3, $codes);
    $this->assertNotEmpty($plugin->dispenseCode());
  }

}
