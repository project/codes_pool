<?php

namespace Drupal\Tests\codes_pool\FunctionalJavascript;

use Drupal\codes_pool\Entity\CodeCollectionType;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests collection entities.
 *
 * @group codes_pool
 */
class CollectionTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'codes_pool',
    'entity',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a test bundle.
    CodeCollectionType::create([
      'id' => 'test',
      'label' => 'Test label',
      'description' => 'Test description',
    ])->save();
  }

  /**
   * Tests codes pool collections.
   */
  public function testCollections() {
    $this->placeBlock('local_tasks_block');
    $this->placeBlock('local_actions_block');
    $this->placeBlock('page_title_block');
    $test_file = \Drupal::service('file_system')->realpath(__DIR__ . '/../../fixtures/codes.csv');
    $assert = $this->assertSession();
    $page = $this->getSession()->getPage();

    $admin = $this->createUser([], NULL, TRUE);
    $admin->addRole('administrator');
    $admin->save();
    $this->drupalLogin($admin);

    $this->drupalGet('admin/structure/codes-pool/collections');
    $assert->pageTextNotContains('New year 20% offer!');

    $page->clickLink('Add code collection');
    $page->fillField('name[0][value]', 'New year 20% offer!');
    $page->fillField('description[0][value]', 'Flat 20% off for your new year purchases.');
    $page->fillField('source[0][target_plugin_id]', 'csv');

    $this->assertSession()->waitForField('files[source_0_target_plugin_configuration_csv_file_upload]');
    $page->attachFileToField('files[source_0_target_plugin_configuration_csv_file_upload]', $test_file);
    $this->assertSession()->waitForText('codes.csv');
    // @todo changing the target plugin and uploading the file reloads the
    //   entire form removing the earlier input.
    $page->fillField('name[0][value]', 'New year 20% offer!');
    $page->fillField('description[0][value]', 'Flat 20% off for your new year purchases.');

    $page->pressButton('Save');
    $this->htmlOutput();

    $assert->pageTextContains('New year 20% offer!');

    $this->drupalGet('admin/structure/codes-pool/collections/1/edit');
    $assert->pageTextContains('New year 20% offer!');
  }

}
