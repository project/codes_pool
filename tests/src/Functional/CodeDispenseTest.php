<?php

namespace Drupal\Tests\codes_pool\Functional;

use Drupal\codes_pool\Entity\CodeCollection;
use Drupal\codes_pool\Entity\CodeCollectionType;
use Drupal\file\Entity\File;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests collection entities.
 *
 * @group codes_pool
 */
class CodeDispenseTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'codes_pool',
    'entity',
    'file',
    'user',
  ];

  /**
   * Test the dispensing of codes.
   */
  public function testCodeDispense() {
    $code_collection_type = CodeCollectionType::create([
      'id' => 'test',
      'label' => 'Test label',
      'description' => 'Test description',
    ]);
    $code_collection_type->save();

    $csv_file = File::create([
      'uri' => \Drupal::service('file_system')->realpath(__DIR__ . '/../../fixtures/codes.csv'),
      'filename' => 'codes.csv',
    ]);
    $csv_file->save();

    // @todo Actually set a bundle.
    $code_collection = CodeCollection::create([
      'status' => TRUE,
      'bundle' => 'test',
      'source' => [
        'target_plugin_id' => 'csv',
        'target_plugin_configuration' => [
          'file_upload' => [$csv_file->id()],
        ],
      ],
    ]);
    $code_collection->save();

    // Login the user.
    $admin = $this->createUser([], NULL, TRUE);
    $admin->addRole('administrator');
    $admin->save();
    $this->drupalLogin($admin);
    $this->drupalGet('admin/structure/codes-pool/collections/' . $code_collection->id() . '/codes/generate');

    // Upload the codes.
    $this->submitForm([], 'Import');
    $this->assertSession()->statusCodeEquals(200);

    $code = $code_collection->dispenseCode();
    $this->assertEquals('CODE0001', $code, 'The code CODE0001 was dispensed.');

    $codes = $code_collection->dispenseCodes(['quantity' => 2]);
    $this->assertCount(2, $codes, '2 codes were dispensed.');
    $this->assertContains('CODE0002', $codes, 'CODE0002 is one of the codes dispensed.');
    $this->assertContains('CODE0003', $codes, 'CODE0003 is one of the codes dispensed.');
  }

}
