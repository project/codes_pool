<?php

namespace Drupal\Tests\codes_pool\Functional;

use Drupal\file\Entity\File;
use Drupal\codes_pool\Entity\CodeCollection;
use Drupal\codes_pool\Entity\CodeCollectionType;
use Drupal\Core\Database\Database;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests collection entities.
 *
 * @group codes_pool
 */
class CodeReUploadTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'codes_pool',
    'entity',
    'file',
    'user',
  ];

  /**
   * Test that non-dispensed codes are deleted when new codes are uploaded.
   */
  public function testDeletionOfNonDispensedCodes() {
    $code_collection_type = CodeCollectionType::create([
      'id' => 'test',
      'label' => 'Test label',
      'description' => 'Test description',
    ]);
    $code_collection_type->save();

    $csv_file = File::create([
      'uri' => \Drupal::service('file_system')->realpath(__DIR__ . '/../../fixtures/codes.csv'),
      'filename' => 'codes.csv',
    ]);
    $csv_file->save();

    // @todo Actually set a bundle.
    $code_collection = CodeCollection::create([
      'status' => TRUE,
      'bundle' => 'test',
      'source' => [
        'target_plugin_id' => 'csv',
        'target_plugin_configuration' => [
          'file_upload' => [$csv_file->id()],
        ],
      ],
    ]);
    $code_collection->save();

    // Login the user.
    $admin = $this->createUser([], NULL, TRUE);
    $admin->addRole('administrator');
    $admin->save();
    $this->drupalLogin($admin);
    $this->drupalGet('admin/structure/codes-pool/collections/' . $code_collection->id() . '/codes/generate');

    // Upload the codes.
    $this->submitForm([], 'Import');
    $this->assertSession()->statusCodeEquals(200);

    // Dispense a few codes.
    $codes = $code_collection->dispenseCodes(['quantity' => 4]);
    $this->assertCount(4, $codes, '4 codes were dispensed.');
    $this->assertContains('CODE0001', $codes, 'CODE0001 is one of the codes dispensed.');
    $this->assertContains('CODE0002', $codes, 'CODE0002 is one of the codes dispensed.');
    $this->assertContains('CODE0003', $codes, 'CODE0003 is one of the codes dispensed.');
    $this->assertContains('CODE0004', $codes, 'CODE0004 is one of the codes dispensed.');
    $this->drupalGet('admin/structure/codes-pool/collections/' . $code_collection->id() . '/codes');
    $this->assertSame(12, $code_collection->getCodesCount());
    $this->assertSame(8, $code_collection->getCountCodeAvailable());

    // Import another set of codes again on the same code collection and test
    // that the existing non-dispensed codes have been properly deleted.
    $csv_file = File::create([
      'uri' => \Drupal::service('file_system')->realpath(__DIR__ . '/../../fixtures/codes_reupload.csv'),
      'filename' => 'codes_reupload.csv',
    ]);
    $csv_file->save();
    $code_collection->set('source', [
      'target_plugin_id' => 'csv',
      'target_plugin_configuration' => [
        'file_upload' => [$csv_file->id()],
      ],
    ])->save();
    $this->drupalGet('admin/structure/codes-pool/collections/' . $code_collection->id() . '/codes/generate');
    $this->submitForm([], 'Import');
    $this->assertSame(9, $code_collection->getCodesCount());
    $this->assertSame(5, $code_collection->getCountCodeAvailable());
  }

  /**
   * Test that duplicates are skipped when new codes are uploaded.
   */
  public function testOnlyUniqueCodesInserted() {
    $code_collection_type = CodeCollectionType::create([
      'id' => 'test',
      'label' => 'Test label',
      'description' => 'Test description',
    ]);
    $code_collection_type->save();

    $csv_file = File::create([
      'uri' => \Drupal::service('file_system')->realpath(__DIR__ . '/../../fixtures/codes.csv'),
      'filename' => 'codes.csv',
    ]);
    $csv_file->save();

    // @todo Actually set a bundle.
    $code_collection = CodeCollection::create([
      'status' => TRUE,
      'bundle' => 'test',
      'source' => [
        'target_plugin_id' => 'csv',
        'target_plugin_configuration' => [
          'file_upload' => [$csv_file->id()],
        ],
      ],
    ]);
    $code_collection->save();

    // Login the user.
    $admin = $this->createUser([], NULL, TRUE);
    $admin->addRole('administrator');
    $admin->save();
    $this->drupalLogin($admin);
    $this->drupalGet('admin/structure/codes-pool/collections/' . $code_collection->id() . '/codes/generate');

    // Upload the codes.
    $this->submitForm([], 'Import');
    $this->assertSession()->statusCodeEquals(200);

    // Dispense a few codes.
    $codes = $code_collection->dispenseCodes(['quantity' => 4]);
    $this->assertCount(4, $codes, '4 codes were dispensed.');
    $this->assertContains('CODE0001', $codes, 'CODE0001 is one of the codes dispensed.');
    $this->assertContains('CODE0002', $codes, 'CODE0002 is one of the codes dispensed.');
    $this->assertContains('CODE0003', $codes, 'CODE0003 is one of the codes dispensed.');
    $this->assertContains('CODE0004', $codes, 'CODE0004 is one of the codes dispensed.');
    $this->drupalGet('admin/structure/codes-pool/collections/' . $code_collection->id() . '/codes');
    $this->assertSame(12, $code_collection->getCodesCount());
    $this->assertSame(8, $code_collection->getCountCodeAvailable());

    // Import another set of codes again on the same code collection and test
    // that the duplicates are not inserted and non-dispensed codes are deleted.
    $csv_file = File::create([
      'uri' => \Drupal::service('file_system')->realpath(__DIR__ . '/../../fixtures/codes_with_duplicates.csv'),
      'filename' => 'codes_with_duplicates.csv',
    ]);
    $csv_file->save();
    $code_collection->set('source', [
      'target_plugin_id' => 'csv',
      'target_plugin_configuration' => [
        'file_upload' => [$csv_file->id()],
      ],
    ])->save();
    $this->drupalGet('admin/structure/codes-pool/collections/' . $code_collection->id() . '/codes/generate');
    $this->submitForm([], 'Import');
    $this->drupalGet('admin/structure/codes-pool/collections/' . $code_collection->id() . '/codes');
    $this->assertSame(7, $code_collection->getCodesCount());
    $this->assertSame(3, $code_collection->getCountCodeAvailable());
    // Now, assert the codes in the database, as well.
    $source = $code_collection->getSourcePlugin();
    /** @var \Drupal\codes_pool\Plugin\codes_pool\CollectionSource\Csv  $source */
    $query = Database::getConnection()->select($source->getTableName(), 'c');
    $codes = $query->fields('c', ['code', 'status'])
      ->condition('c.entity_id', $code_collection->id())
      ->execute()
      ->fetchAllKeyed();
    $expected_codes = [
      'CODE0001' => '1',
      'CODE0002' => '1',
      'CODE0003' => '1',
      'CODE0004' => '1',
      'CODE2001' => '0',
      'CODE2002' => '0',
      'CODE2003' => '0',
    ];
    foreach ($codes as $code => $status) {
      $this->assertArrayHasKey($code, $expected_codes);
      $expected_status = $expected_codes[$code];
      $this->assertSame($expected_status, $status);
    }
  }

}
