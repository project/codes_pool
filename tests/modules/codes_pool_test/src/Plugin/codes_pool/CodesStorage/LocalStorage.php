<?php

namespace Drupal\codes_pool_test\Plugin\codes_pool\CodesStorage;

use Drupal\codes_pool\Plugin\codes_pool\CodesStorage\CodesStorageBase;

/**
 * Database storage.
 *
 * @CodesStorage(
 *   id = "codes_pool_testing_local_storage",
 *   label = @Translation("Testing Codes Pool Local Storage")
 * )
 */
class LocalStorage extends CodesStorageBase {

  /**
   * {@inheritdoc}
   */
  public function dispenseCode(array $params = [], ?int $entity_id = NULL): string {
    $codes = $this->dispenseCodes($params, $entity_id);

    return $codes[0];
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCodes(array $params = [], ?int $entity_id = NULL): array {
    $codes = [];

    $quantity = $params['quantity'] ?? 1;
    $length = $params['length'] ?? 11;

    while ($quantity > 0) {
      // Generates a unique string of specified length.
      $codes[] = substr(str_replace('.', '', uniqid("", TRUE)), -$length);

      $quantity--;
    }

    return $codes;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNonDispensedCodes(int $entity_id): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function codeIsUnique(string $code, string $bundle): bool {
    return TRUE;
  }

}
