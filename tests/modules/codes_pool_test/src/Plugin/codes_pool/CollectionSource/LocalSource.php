<?php

namespace Drupal\codes_pool_test\Plugin\codes_pool\CollectionSource;

use Drupal\codes_pool\Plugin\codes_pool\CodesStorage\CodesStorageInterface;
use Drupal\codes_pool\Plugin\codes_pool\CollectionSource\CollectionSourceBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Local source for collection.
 *
 * @CodeCollectionSource(
 *   id = "codes_pool_testing_local_source",
 *   label = @Translation("Testing Codes Pool Local Source"),
 *   entity_type = "codes_pool_collection",
 *   storage = "codes_pool_testing_local_storage"
 * )
 */
class LocalSource extends CollectionSourceBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += parent::buildConfigurationForm($form, $form_state);

    // Clear config form.
    $form['#type'] = 'container';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCount(?int $entity_id = NULL): int {
    return CodesStorageInterface::CODE_COUNT_UNLIMITED;
  }

  /**
   * {@inheritdoc}
   */
  public function getCountCodeAvailable(?int $entity_id = NULL): int {
    return CodesStorageInterface::CODE_COUNT_UNLIMITED;
  }

  /**
   * {@inheritdoc}
   */
  public function hasCodeAvailable(?int $entity_id = NULL): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCode(array $params = [], ?int $entity_id = NULL): string {
    // Set quantity to 1.
    $params['quantity'] = 1;

    return $this->getCodesStorage()->dispenseCode($params, $entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCodes(array $params = [], ?int $entity_id = NULL): array {
    return $this->getCodesStorage()->dispenseCodes($params, $entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNonDispensedCodes(int $entity_id): bool {
    return $this->getCodesStorage()->deleteNonDispensedCodes($entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function codeIsUnique(string $code, string $bundle): bool {
    return $this->getCodesStorage()->codeIsUnique($code, $bundle);
  }

}
