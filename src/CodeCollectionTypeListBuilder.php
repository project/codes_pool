<?php

namespace Drupal\codes_pool;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of code collection type entities.
 *
 * @see \Drupal\codes_pool\Entity\CodeCollectionType
 */
class CodeCollectionTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Label');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No code collection type available. <a href=":link">Add code collection type</a>.',
      [':link' => Url::fromRoute('entity.codes_pool_collection_type.add_form')->toString()]
    );

    return $build;
  }

}
