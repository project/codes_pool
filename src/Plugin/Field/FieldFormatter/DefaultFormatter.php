<?php

namespace Drupal\codes_pool\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'codes_pool_item_default' formatter.
 *
 * @FieldFormatter(
 *   id = "codes_pool_item_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "codes_pool_item"
 *   }
 * )
 */
class DefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $target_definition = $item->getTargetDefinition();
      if (!empty($target_definition['label'])) {
        $elements[$delta] = [
          '#markup' => $target_definition['label'],
        ];
      }
      else {
        $elements[$delta] = [
          '#markup' => $target_definition['id'],
        ];
      }
    }

    return $elements;
  }

}
