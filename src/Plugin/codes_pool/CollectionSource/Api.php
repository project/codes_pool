<?php

namespace Drupal\codes_pool\Plugin\codes_pool\CollectionSource;

use Drupal\codes_pool\Plugin\codes_pool\CodesStorage\CodesStorageInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the CSV source for collection.
 *
 * @CodeCollectionSource(
 *   id = "api",
 *   label = @Translation("API"),
 *   entity_type = "codes_pool_collection",
 *   storage = "remote"
 * )
 */
class Api extends CollectionSourceBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'endpoint_uri' => NULL,
      'authentication_provider' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += parent::buildConfigurationForm($form, $form_state);
    // Remove the main fieldset.
    $form['#type'] = 'container';

    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API'),
      '#collapsible' => FALSE,
    ];
    $form['api']['uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#default_value' => $this->configuration['endpoint_uri'],
    ];
    $form['api']['authentication'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Authentication'),
      '#collapsible' => TRUE,
    ];
    $form['api']['authentication']['provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Provider'),
      '#default_value' => $this->configuration['authentication_provider'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['endpoint_uri'] = $values['api']['endpoint']['uri'];
      $this->configuration['authentication_provider'] = $values['api']['authentication']['provider'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($entity_id) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCount(?int $entity_id = NULL): int {
    // @todo Implement getCount() method.
    return CodesStorageInterface::CODE_COUNT_UNLIMITED;
  }

  /**
   * {@inheritdoc}
   */
  public function getCountCodeAvailable(?int $entity_id = NULL): int {
    return CodesStorageInterface::CODE_COUNT_UNLIMITED;
  }

  /**
   * {@inheritdoc}
   */
  public function hasCodeAvailable(?int $entity_id = NULL): bool {
    // @todo make sure API is alive.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCode(array $params = [], ?int $entity_id = NULL): string {
    // @todo call API to get a code.
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCodes(array $params = [], ?int $entity_id = NULL): array {
    // @todo call API to get a code.
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNonDispensedCodes(int $entity_id): bool {
    // @todo Implement API call to delete non-dispensed codes.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function codeIsUnique(string $code, string $bundle): bool {
    // @todo Implement API call to check that the code is unique.
    return TRUE;
  }

}
