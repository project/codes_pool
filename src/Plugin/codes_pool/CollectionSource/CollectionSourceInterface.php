<?php

namespace Drupal\codes_pool\Plugin\codes_pool\CollectionSource;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the base interface for collection.
 */
interface CollectionSourceInterface extends ConfigurableInterface, PluginFormInterface, PluginInspectionInterface {

  /**
   * Gets the collection entity type ID.
   *
   * @return string
   *   The collection's entity type ID.
   */
  public function getEntityTypeId(): string;

  /**
   * Checks if the codes can be stored locally.
   *
   * @return bool
   *   TRUE if stored, FALSE otherwise.
   */
  public function persistCodes(): bool;

  /**
   * Provides total code count.
   *
   * @param int|null $entity_id
   *   An entity ID to get the count for, if one exists.
   *
   * @return int
   *   Number of codes.
   */
  public function getCount(?int $entity_id = NULL): int;

  /**
   * Provides total code count to dispense.
   *
   * @param int|null $entity_id
   *   An entity ID to get the count for, if one exists.
   *
   * @return int
   *   Number of codes.
   */
  public function getCountCodeAvailable(?int $entity_id = NULL): int;

  /**
   * Checks if there is any code available to dispense.
   *
   * @param int|null $entity_id
   *   An entity ID to get the count for, if one exists.
   *
   * @return bool
   *   TRUE if available. FALSE otherwise.
   */
  public function hasCodeAvailable(?int $entity_id = NULL): bool;

  /**
   * Dispense a code.
   *
   * @param array $params
   *   Optional information for the dispenseCodes implementation.
   * @param int|null $entity_id
   *   An entity ID to get the count for, if one exists.
   *
   * @return string
   *   The code.
   */
  public function dispenseCode(array $params = [], ?int $entity_id = NULL): string;

  /**
   * Dispense codes.
   *
   * @param array $params
   *   Optional information for the dispenseCodes implementation.
   * @param int|null $entity_id
   *   An entity ID to get the count for, if one exists.
   *
   * @return array
   *   The array of codes.
   */
  public function dispenseCodes(array $params = [], ?int $entity_id = NULL): array;

  /**
   * Deletes the non-dispensed codes.
   *
   * @param int $entity_id
   *   The entity ID to delete the codes for.
   *
   * @return bool
   *   TRUE if the deletion was successful. FALSE otherwise.
   */
  public function deleteNonDispensedCodes(int $entity_id): bool;

  /**
   * Checks if a code is unique across a bundle.
   *
   * @param string $code
   *   The code to check for.
   * @param string $bundle
   *   The bundle name to check for.
   *
   * @return bool
   *   TRUE if the code is unique.
   */
  public function codeIsUnique(string $code, string $bundle): bool;

}
