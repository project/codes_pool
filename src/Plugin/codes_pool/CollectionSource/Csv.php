<?php

namespace Drupal\codes_pool\Plugin\codes_pool\CollectionSource;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides the CSV source for collection.
 *
 * @CodeCollectionSource(
 *   id = "csv",
 *   label = @Translation("CSV"),
 *   entity_type = "codes_pool_collection",
 *   storage = "database"
 * )
 */
class Csv extends CollectionSourceBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'file_upload' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += parent::buildConfigurationForm($form, $form_state);
    // Remove the main fieldset.
    $form['#type'] = 'container';

    $form['file'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration'),
      '#collapsible' => FALSE,
    ];
    $form['file']['upload'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('File'),
      '#upload_location' => 'public://codes_pool/collection' ,
      '#default_value' => $this->configuration['file_upload'],
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
        // @todo: Add other validations.
      ],
      '#required' => TRUE,
    ];

    $form['file']['metadata'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Metadata'),
      '#description' => $this->t('Provide column details in index:field format.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['file_upload'] = [$values['file']['upload'][0]];
    }
  }

  /**
   * Post save.
   */
  public function postSave() {
    /** @var \Drupal\codes_pool\Plugin\codes_pool\CodesStorage\Database $codes_storage */
    $codes_storage = $this->getCodesStorage();
    // Create table.
    $codes_storage->createStorage($this->getTableName());
  }

  /**
   * {@inheritdoc}
   */
  public function import() {
    // If there is no file, do not proceed.
    $fid = $this->configuration['file_upload'];
    if (!$fid) {
      return TRUE;
    }

    /** @var \Drupal\file\Entity\File $file */
    $file = File::load(reset($fid));

    /** @var \Drupal\codes_pool\Plugin\codes_pool\CodesStorage\Database $codes_storage */
    $codes_storage = $this->getCodesStorage();
    $storage_class = get_class($codes_storage);
    $table = $this->getTableName();

    // Setup batch operation for insert.
    $batch = [
      'operations' => [],
      'finished' => $this->t('Finished code import.'),
      'title' => $this->t('Importing codes...'),
      'init_message' => $this->t('Starting code import.'),
      'progress_message' => $this->t('Completed @current step of @total.'),
      'error_message' => $this->t('Codes import has encountered an error.'),
    ];

    $handle = fopen($file->getFileUri(), 'r');
    $fields = ['entity_id', 'code', 'created', 'changed', 'status'];
    $duplicate_codes = [];
    $all_processed_codes = [];
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
      // Check if this code already exists for this bundle, if so, skip it.
      // The duplicates could already be in the database or there could be
      // duplicates within the uploaded CSV, in either case, skip it.
      if (!$this->codeIsUnique($data[0], $this->entity->bundle()) || isset($all_processed_codes[$data[0]])) {
        // Add the duplicate codes to an array for reporting purposes later.
        $duplicate_codes[] = $data[0];
        continue;
      }

      $row['entity_id'] = $this->entity->id();
      // @todo use metadata from the entity here. Right now, it is just code.
      $row['code'] = $data[0];
      $row['created'] = time();
      $row['changed'] = $row['created'];
      $row['status'] = 0;
      $rows[] = $row;
      if (count($rows) >= 100) {
        $batch['operations'][] = [
          [$storage_class, 'insert'],
          [$table, $fields, $rows],
        ];
        $rows = [];
      }
      $all_processed_codes[$data[0]] = $data[0];
    }
    fclose($handle);

    // Add last batch.
    if (!empty($rows)) {
      $batch['operations'][] = [
        [$storage_class, 'insert'],
        [$table, $fields, $rows],
      ];
    }

    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public function persistCodes(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCountCodeAvailable(?int $entity_id = NULL): int {
    return $this->getCodesStorage()->countAvailable($entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getCount(?int $entity_id = NULL): int {
    return $this->getCodesStorage()->count($entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function hasCodeAvailable(?int $entity_id = NULL): bool {
    return $this->getCountCodeAvailable($entity_id) > 0;
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCode(array $params = [], ?int $entity_id = NULL): string {
    return $this->getCodesStorage()->dispenseCode($params, $entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCodes(array $params = [], ?int $entity_id = NULL): array {
    return $this->getCodesStorage()->dispenseCodes($params, $entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNonDispensedCodes(int $entity_id): bool {
    return $this->getCodesStorage()->deleteNonDispensedCodes($entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function codeIsUnique(string $code, string $bundle): bool {
    return $this->getCodesStorage()->codeIsUnique($code, $bundle);
  }

  /**
   * Provides table name of the entity.
   *
   * @return string
   *   The table name.
   */
  public function getTableName() {
    return 'codes_pool_csv_' . $this->entity->bundle();
  }

  /**
   * {@inheritdoc}
   */
  protected function getCodesStorage() {
    $storage = parent::getCodesStorage();
    $storage->setTableName($this->getTableName());
    return $storage;
  }

}
