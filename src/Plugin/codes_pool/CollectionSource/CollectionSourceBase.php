<?php

namespace Drupal\codes_pool\Plugin\codes_pool\CollectionSource;

use Drupal\codes_pool\CodesStorageManager;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the base class for offers.
 */
abstract class CollectionSourceBase extends PluginBase implements CollectionSourceInterface, ContainerFactoryPluginInterface {

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Code storage manager.
   *
   * @var \Drupal\codes_pool\CodesStorageManager
   */
  protected $codesStorageManager;

  /**
   * Constructs a new CollectionSourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The pluginId for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\codes_pool\CodesStorageManager $codes_storage_manager
   *   Codes storage manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CodesStorageManager $codes_storage_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->codesStorageManager = $codes_storage_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.codes_pool_codes_storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeepArray([
      $this->defaultConfiguration(),
      $configuration,
    ], TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Wrap the offer configuration in a fieldset by default.
    $form['#type'] = 'fieldset';
    $form['#title'] = $this->t('Settings');
    $form['#collapsible'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $this->configuration = [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    return $this->entity = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId(): string {
    return $this->pluginDefinition['entity_type'];
  }

  /**
   * Asserts that the given entity is of the expected type.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  protected function assertEntity(EntityInterface $entity) {
    $entity_type_id = $entity->getEntityTypeId();
    $offer_entity_type_id = $this->getEntityTypeId();
    if ($entity_type_id != $offer_entity_type_id) {
      throw new \InvalidArgumentException(sprintf('The offer requires a "%s" entity, but a "%s" entity was given.', $offer_entity_type_id, $entity_type_id));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function persistCodes(): bool {
    return FALSE;
  }

  /**
   * Provides storage.
   *
   * @return \Drupal\codes_pool\Plugin\codes_pool\CodesStorage\CodesStorageInterface
   *   Storage object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getCodesStorage() {
    $storage = $this->codesStorageManager->createInstance($this->pluginDefinition['storage']);
    return $storage;
  }

}
