<?php

namespace Drupal\codes_pool\Plugin\codes_pool\CodesStorage;

use Drupal\Core\Database\SchemaObjectExistsException;

/**
 * Internal code storage plugin.
 *
 * @CodesStorage(
 *   id = "database",
 *   label = @Translation("Database")
 * )
 */
class Database implements CodesStorageInterface {

  /**
   * The connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Table name.
   *
   * @var string
   */
  protected $tableName;

  /**
   * Creates table.
   *
   * @param string $table_name
   *   Table name.
   */
  public function createStorage($table_name) {
    try {
      $this->getConnection()->schema()->createTable($table_name, $this->schemaDefinition());
      $this->tableName = $table_name;
    }
    catch (SchemaObjectExistsException $e) {
      // Do not throw error, if table already exist.
    }
  }

  /**
   * Sets table name.
   *
   * @param string $table_name
   *   Table name.
   */
  public function setTableName($table_name) {
    $this->tableName = $table_name;
  }

  /**
   * Provides database connection.
   *
   * @return \Drupal\Core\Database\Connection
   *   Connection object.
   */
  protected  function getConnection() {
    if (!$this->connection) {
      $this->connection = \Drupal::database();
    }
    return $this->connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function insert($table, $fields, $rows) {
    $query = \Drupal::database()
      ->insert($table)
      ->fields($fields);
    foreach ($rows as $row) {
      $query->values($row);
    }
    return $query->execute();
  }

  /**
   * Count of total row.
   *
   * @param int $entity_id
   *   The code collection entity ID.
   *
   * @return int
   *   The total count.
   */
  public function count(int $entity_id) {
    return $this->getConnection()
      ->select($this->tableName, 'c')
      ->condition('c.entity_id', $entity_id)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * Count of available code.
   *
   * @param int $entity_id
   *   The code collection entity ID.
   *
   * @return int
   *   The count.
   */
  public function countAvailable(int $entity_id) {
    $query = $this->getConnection()
      ->select($this->tableName, 'c')
      ->condition('c.entity_id', $entity_id)
      ->condition('c.status', 0);
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function hasImport(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCode(array $params = [], ?int $entity_id = NULL): string {
    $result = $this->getConnection()->select($this->tableName, 'c')
      ->fields('c', ['id', 'code'])
      ->condition('c.entity_id', $entity_id)
      ->condition('c.status', 0)
      ->orderBy('c.id')
      ->forUpdate(TRUE)
      ->range(0, 1)
      ->execute()
      ->fetch();
    $this->getConnection()->update($this->tableName)
      ->fields(['status' => 1])
      ->condition('id', $result->id)
      ->execute();
    return $result->code;
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCodes(array $params = [], ?int $entity_id = NULL): array {
    $results = $this->getConnection()->select($this->tableName, 'c')
      ->fields('c', ['id', 'code'])
      ->condition('c.entity_id', $entity_id)
      ->condition('c.status', 0)
      ->orderBy('c.id')
      ->forUpdate(TRUE)
      ->range(0, $params['quantity'] ?? 1)
      ->execute()
      ->fetchAll();

    $codes = [];
    foreach ($results as $result) {
      $this->getConnection()->update($this->tableName)
        ->fields(['status' => 1])
        ->condition('id', $result->id)
        ->execute();

      $codes[] = $result->code;
    }

    return $codes;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNonDispensedCodes(int $entity_id): bool {
    return $this->getConnection()->delete($this->tableName)
      ->condition('entity_id', $entity_id)
      ->condition('status', 0)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function codeIsUnique(string $code, string $bundle): bool {
    $query = $this->getConnection()
      ->select($this->tableName, 'c')
      ->condition('c.code', $code);
    $exists = $query->countQuery()->execute()->fetchField();

    return !$exists;
  }

  /**
   * Defines the schema for the {codes_pool_code_[ID]} table.
   *
   * @internal
   */
  public function schemaDefinition() {
    return [
      'description' => 'Codes pool table.',
      'fields' => [
        'id' => [
          'type' => 'serial',
          'not null' => TRUE,
          'unsigned' => TRUE,
          'description' => 'Unique ID.',
        ],
        'entity_id' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'description' => 'The entity_id of which the code belong.',
        ],
        'code' => [
          'type' => 'varchar',
          'length' => 50,
          'not null' => TRUE,
          'description' => 'Individual code',
        ],
        'status' => [
          'type' => 'int',
          'length' => 4,
          'default' => 0,
          'description' => 'Status',
        ],
        'created' => [
          'type' => 'int',
          'description' => 'Created timestamp',
        ],
        'changed' => [
          'description' => 'Changed timestamp',
          'type' => 'int',
        ],
      ],
      'primary key' => ['id'],
      'unique keys' => [
        'id' => ['id'],
      ],
      'indexes' => [
        'status' => ['status'],
        'entity_id_status' => ['entity_id', 'status'],
        'code' => ['code']
      ],
    ];
  }

}
