<?php

namespace Drupal\codes_pool\Plugin\codes_pool\CodesStorage;

/**
 * Defines the base interface for collection.
 */
interface CodesStorageInterface {

  /**
   * Unlimited code.
   */
  const CODE_COUNT_UNLIMITED = -1;

  /**
   * Checks import is possible.
   *
   * @return bool
   *   TRUE if possible. FALSE otherwise.
   */
  public function hasImport(): bool;

  /**
   * Dispense a code.
   *
   * @param array $params
   *   Optional information for the dispenseCode implementation.
   * @param int|null $entity_id
   *   An entity ID to get the count for, if one exists.
   *
   * @return string
   *   The code.
   */
  public function dispenseCode(array $params = [], ?int $entity_id = NULL): string;

  /**
   * Dispense codes.
   *
   * @param array $params
   *   Optional information for the dispenseCodes implementation.
   * @param int|null $entity_id
   *   An entity ID to get the count for, if one exists.
   *
   * @return array
   *   The array of codes.
   */
  public function dispenseCodes(array $params = [], ?int $entity_id = NULL) : array;

  /**
   * Deletes the non-dispensed codes.
   *
   * @param int $entity_id
   *   The entity ID to delete the codes for.
   *
   * @return bool
   *   TRUE if the deletion was successful. FALSE otherwise.
   */
  public function deleteNonDispensedCodes(int $entity_id): bool;

  /**
   * Checks if a code is unique across a bundle.
   *
   * @param string $code
   *   The code to check for.
   * @param string $bundle
   *   The bundle name to check for.
   *
   * @return bool
   *   TRUE if the code is unique.
   */
  public function codeIsUnique(string $code, string $bundle): bool;

}
