<?php

namespace Drupal\codes_pool\Plugin\codes_pool\CodesStorage;

/**
 * Database storage.
 *
 * @CodesStorage(
 *   id = "remote",
 *   label = @Translation("Remote")
 * )
 */
class Remote extends CodesStorageBase {

  /**
   * {@inheritdoc}
   */
  public function dispenseCode(array $params = [], ?int $entity_id = NULL): string {
    // @todo Implement API call to get code.
  }

  /**
   * {@inheritdoc}
   */
  public function dispenseCodes(array $params = [], ?int $entity_id = NULL): array {
    // @todo Implement API call to get code.
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNonDispensedCodes(int $entity_id): bool {
    // @todo Implement API call to delete non-dispensed codes.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function codeIsUnique(string $code, string $bundle): bool {
    // @todo Implement API call to check that the code is unique.
    return TRUE;
  }

}
