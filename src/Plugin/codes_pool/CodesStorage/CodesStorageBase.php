<?php

namespace Drupal\codes_pool\Plugin\codes_pool\CodesStorage;

/**
 * Base class for the code storage.
 *
 * @package Drupal\codes_pool\Plugin\codes_pool\CodesStorage
 */
abstract class CodesStorageBase implements CodesStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function hasImport(): bool {
    return FALSE;
  }

}
