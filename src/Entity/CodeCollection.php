<?php

namespace Drupal\codes_pool\Entity;

use Drupal\codes_pool\Plugin\codes_pool\CollectionSource\CollectionSourceInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Defines the code collection entity class.
 *
 * @ContentEntityType(
 *   id = "codes_pool_collection",
 *   label = @Translation("Code collection"),
 *   label_collection = @Translation("Code collections"),
 *   label_singular = @Translation("Code collection"),
 *   label_plural = @Translation("Code collections"),
 *   label_count = @PluralTranslation(
 *     singular = "@count code collection",
 *     plural = "@count code collections",
 *     context = "Codes pool",
 *   ),
 *   bundle_label = @Translation("Code collection type"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\codes_pool\CodeCollectionListBuilder",
 *     "views_data" = "Drupal\codes_pool\CodeCollectionViewsData",
 *     "form" = {
 *       "default" = "Drupal\codes_pool\Form\CodeCollectionForm",
 *       "add" = "Drupal\codes_pool\Form\CodeCollectionForm",
 *       "edit" = "Drupal\codes_pool\Form\CodeCollectionForm",
 *       "duplicate" = "Drupal\codes_pool\Form\CodeCollectionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler"
 *   },
 *   base_table = "codes_pool_collection",
 *   data_table = "codes_pool_collection_field_data",
 *   admin_permission = "administer codes pool collections",
 *   permission_granularity = "bundle",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "collection_id",
 *     "bundle" = "bundle",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/codes-pool/collections/add/{codes_pool_collection_type}",
 *     "add-page" = "/admin/structure/codes-pool/collections/add",
 *     "edit-form" = "/admin/structure/codes-pool/collections/{codes_pool_collection}/edit",
 *     "duplicate-form" = "/admin/structure/codes-pool/collections/{codes_pool_collection}/duplicate",
 *     "delete-form" = "/admin/structure/codes-pool/collections/{codes_pool_collection}/delete",
 *     "collection" = "/admin/structure/codes-pool/collections",
 *   },
 *   bundle_entity_type = "codes_pool_collection_type",
 *   field_ui_base_route = "entity.codes_pool_collection_type.edit_form",
 * )
 */
class CodeCollection extends ContentEntityBase implements CodeCollectionInterface {

  /**
   * Code source.
   *
   * @var \Drupal\codes_pool\Plugin\codes_pool\CollectionSource\CollectionSourceInterface
   */
  protected $sourcePlugin;

  /**
   * {@inheritdoc}
   */
  public function createDuplicate() {
    $duplicate = parent::createDuplicate();
    // Source cannot be transferred because their codes are unique.
    $duplicate->set('source', []);

    return $duplicate;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * Gets source plugin object.
   *
   * @return \Drupal\codes_pool\Plugin\codes_pool\CollectionSource\CollectionSourceInterface
   *   Collection source object.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getSourcePlugin() {
    if (!$this->get('source')->isEmpty() && !$this->sourcePlugin) {
      $this->sourcePlugin = $this->get('source')->first()->getTargetInstance();
      $this->sourcePlugin->setEntity($this);
    }
    return $this->sourcePlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setSource(CollectionSourceInterface $source) {
    $this->set('source', [
      'target_plugin_id' => $source->getPluginId(),
      'target_plugin_configuration' => $source->getConfiguration(),
    ]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled($enabled) {
    $this->set('status', (bool) $enabled);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return (int) $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Call only on collection create.
    if (!$update) {
      // @todo: add event to react to this case.
      $this->getSourcePlugin()->postSave();
    }
    else {
      // @todo: Check if source has changed on update, if so,
      // delete all non-dispensed and insert new codes.
      // @todo: add event to react to this case.
    }
  }

  /**
   * Provides total code count.
   *
   * @return int
   *   Number of codes.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getCodesCount() {
    return $this->getSourcePlugin()->getCount($this->id());
  }

  /**
   * Provides total code count to dispense.
   *
   * @return int
   *   Number of codes.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getCountCodeAvailable() {
    return $this->getSourcePlugin()->getCountCodeAvailable($this->id());

  }

  /**
   * Checks if there any code available to dispense.
   *
   * @return bool
   *   TRUE if available. FALSE otherwise.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function hasCodeAvailable() {
    return $this->getSourcePlugin()->hasCodeAvailable($this->id());
  }

  /**
   * Dispense a code.
   *
   * @param array $params
   *   Optional information for the dispenseCode implementation.
   *
   * @return string
   *   The code.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function dispenseCode(array $params = []) {
    return $this->getSourcePlugin()->dispenseCode($params, $this->id());
  }

  /**
   * Dispense an array of codes.
   *
   * @param array $params
   *   Optional information for the dispenseCode implementation.
   *
   * @return array
   *   The array of codes.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function dispenseCodes(array $params = []) {
    return $this->getSourcePlugin()->dispenseCodes($params, $this->id());
  }

  /**
   * Load code details by code.
   *
   * @param string $code
   *   String code.
   */
  public function loadByCode($code) {
    // @todo: load code and collection details.
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    /** @var \Drupal\codes_pool\Entity\CodeCollection $entity */
    foreach ($entities as $entity) {
      $source = $entity->getSourcePlugin();
      if (!$source->persistCodes()) {
        continue;
      }
      // @todo: Delete the all the codes imported as part of the collection.
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The code collection name.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Additional information about the promotion to show to the customer'))
      ->setTranslatable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 1,
        'settings' => [
          'rows' => 3,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['source'] = BaseFieldDefinition::create('codes_pool_collection_source')
      ->setLabel(t('Source'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'codes_pool_select',
        'weight' => 2,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('Whether the collection is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSettings([
        'on_label' => t('Active'),
        'off_label' => t('Inactive'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 3,
      ]);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this collection in relation to others.'))
      ->setDefaultValue(0);

    return $fields;
  }

  /**
   * Default value callback for 'start_date' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return string
   *   The default value (date string).
   */
  public static function getDefaultStartDate() {
    $timestamp = \Drupal::time()->getRequestTime();
    return gmdate(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $timestamp);
  }

  /**
   * Helper callback for uasort() to sort code collections by weight and label.
   *
   * @param \Drupal\codes_pool\Entity\CodeCollectionInterface $a
   *   The first collection to sort.
   * @param \Drupal\codes_pool\Entity\CodeCollectionInterface $b
   *   The second collection to sort.
   *
   * @return int
   *   The comparison result for uasort().
   */
  public static function sort(CodeCollectionInterface $a, CodeCollectionInterface $b) {
    $a_weight = $a->getWeight();
    $b_weight = $b->getWeight();
    if ($a_weight == $b_weight) {
      $a_label = $a->label();
      $b_label = $b->label();
      return strnatcasecmp($a_label, $b_label);
    }
    return ($a_weight < $b_weight) ? -1 : 1;
  }

}
