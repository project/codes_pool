<?php

namespace Drupal\codes_pool\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the interface for code collection.
 */
interface CodeCollectionInterface extends ContentEntityInterface {

  /**
   * Gets the collection name.
   *
   * @return string
   *   The collection name.
   */
  public function getName();

  /**
   * Sets the collection name.
   *
   * @param string $name
   *   The collection name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Gets the collection description.
   *
   * @return string
   *   The collection description.
   */
  public function getDescription();

  /**
   * Sets the collection description.
   *
   * @param string $description
   *   The collection description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Get whether the collection is enabled.
   *
   * @return bool
   *   TRUE if the promotion is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets whether the collection is enabled.
   *
   * @param bool $enabled
   *   Whether the promotion is enabled.
   *
   * @return $this
   */
  public function setEnabled($enabled);

  /**
   * Gets the weight.
   *
   * @return int
   *   The weight.
   */
  public function getWeight();

  /**
   * Sets the weight.
   *
   * @param int $weight
   *   The weight.
   *
   * @return $this
   */
  public function setWeight($weight);

}
