<?php

namespace Drupal\codes_pool\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the code collection  type entity class.
 *
 * @ConfigEntityType(
 *   id = "codes_pool_collection_type",
 *   label = @Translation("Code collection type"),
 *   label_collection = @Translation("Code collection types"),
 *   label_singular = @Translation("Code collection type"),
 *   label_plural = @Translation("Code collection types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count code collection type",
 *     plural = "@count code collection types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\codes_pool\CodeCollectionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\codes_pool\Form\CodeCollectionTypeForm",
 *       "edit" = "Drupal\codes_pool\Form\CodeCollectionTypeForm",
 *       "duplicate" = "Drupal\codes_pool\Form\CodeCollectionTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "type",
 *   admin_permission = "administer codes pool collection types",
 *   bundle_of = "codes_pool_collection",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/codes-pool/collection-types/add",
 *     "edit-form" = "/admin/config/codes-pool/collection-types/{codes_pool_collection_type}/edit",
 *     "duplicate-form" = "/admin/config/codes-pool/collection-types/{codes_pool_collection_type}/duplicate",
 *     "delete-form" = "/admin/config/codes-pool/collection-types/{codes_pool_collection_type}/delete",
 *     "collection" = "/admin/config/codes-pool/collection-types"
 *   }
 * )
 */
class CodeCollectionType extends ConfigEntityBundleBase implements ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * The machine name of this collection type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the code collection type.
   *
   * @var string
   */
  protected $label;

  /**
   * The collection type description.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
