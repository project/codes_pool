<?php

namespace Drupal\codes_pool\Controller;

use Drupal\codes_pool\Entity\CodeCollection;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Datetime\DateFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for System routes.
 */
class CodesController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a TrackerController object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   */
  public function __construct(Connection $database, DateFormatterInterface $dateFormatter) {
    $this->database = $database;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('date.formatter')
    );
  }

  /**
   * Lists codes.
   *
   * @param \Drupal\codes_pool\Entity\CodeCollection $codes_pool_collection
   *   Collection entity.
   *
   * @return array
   *   Render array of codes table.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function list(CodeCollection $codes_pool_collection) {
    $source = $codes_pool_collection->getSourcePlugin();
    // If not stored locally, return.
    if (!$source->persistCodes()) {
      $page['message'] = [
        '#markup' => 'Codes are not stored on the site.!',
      ];
      return $page;
    }
    /** @var \Drupal\codes_pool\Plugin\codes_pool\CollectionSource\Csv  $source */
    $query = $this->database->select($source->getTableName(), 'c')
      ->extend(PagerSelectExtender::class);
    $codes = $query->fields('c', ['id', 'code', 'status', 'created', 'changed'])
      ->condition('c.entity_id', $codes_pool_collection->id())
      ->orderBy('c.changed', 'DESC')
      ->limit(10)
      ->execute()
      ->fetchAllAssoc('id');
    $rows = [];
    if (!empty($codes)) {
      foreach ($codes as $code) {
        $row = [
          'title' => [
            'data' => $this->t('@code', ['@code' => $code->code]),
          ],
          'status' => [
            'data' => $this->t('@status', ['@status' => $code->status]),
          ],
          'created' => [
            'data' => $this->t('@time', ['@time' => $this->dateFormatter->format($code->created)]),
          ],
          'last updated' => [
            'data' => $this->t('@time', ['@time' => $this->dateFormatter->format($code->changed)]),
          ],
        ];
        $rows[] = $row;
      }
    }

    $page['codes'] = [
      '#rows' => $rows,
      '#header' => [
        $this->t('Code'),
        $this->t('Status'),
        $this->t('Created'),
        $this->t('Last updated'),
      ],
      '#type' => 'table',
      '#empty' => $this->t('No codes available.'),
    ];
    $page['pager'] = [
      '#type' => 'pager',
      '#weight' => 10,
    ];
    $page['#sorted'] = TRUE;

    return $page;
  }

}
