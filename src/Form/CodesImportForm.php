<?php

namespace Drupal\codes_pool\Form;

use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\codes_pool\Entity\CodeCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for importing codes.
 *
 * @internal
 */
class CodesImportForm extends ConfirmFormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a CodesImportForm object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
    );
  }

  /**
   * Collection.
   *
   * @var \Drupal\codes_pool\Entity\CodeCollection
   */
  protected $collection;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codes_pool_codes_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.codes_pool_collection.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to import codes');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, CodeCollection $codes_pool_collection = NULL) {
    $form = parent::buildForm($form, $form_state);
    $this->collection = $codes_pool_collection;
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $source = $this->collection->getSourcePlugin();
    if ($source->persistCodes()) {
      // Put this in a transaction so that, in case, anything goes wrong, the
      // data will be rolled back to the previous state.
      $transaction = $this->database->startTransaction();

      // Delete all non-dispensed codes and insert new codes.
      try {
        // Delete non-dispensed codes.
        $source->deleteNonDispensedCodes($this->collection->id());

        // @todo: add event to react to this case.
        $source->import();

        // @todo: add event to react to this case.
        $this->messenger()->addMessage($this->t('Imported codes successfully.'));
        $form_state->setRedirect('entity.codes_pool_collection.collection');
      }
      catch (\Exception $e) {
        $transaction->rollBack();
        throw new \Exception($e->getMessage(), $e->getCode());
      }
    }
    else {
      $this->messenger()->addError($this->t('There is no codes to import.'));
    }

  }

}
