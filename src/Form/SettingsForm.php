<?php

namespace Drupal\codes_pool\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form for configuring the Codes Pool module.
 *
 * @internal
 *   Form classes are internal.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['codes_pool.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codes_pool_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['upload_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Code upload location'),
      '#default_value' => $this->config('codes_pool.settings')->get('upload_location'),
      '#description' => $this->t('Location of CSV code file uploaded.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('codes_pool.settings')
      ->set('upload_location', (bool) $form_state->getValue('upload_location'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
