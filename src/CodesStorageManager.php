<?php

namespace Drupal\codes_pool;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Manages discovery and instantiation of codes storage.
 *
 * @see \Drupal\codes_pool\Annotation\CodesStorage
 * @see plugin_api
 */
class CodesStorageManager extends DefaultPluginManager {

  /**
   * Constructs a new CodesStorageManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/codes_pool/CodesStorage', $namespaces, $module_handler, 'Drupal\codes_pool\Plugin\codes_pool\CodesStorage\CodesStorageInterface', 'Drupal\codes_pool\Annotation\CodesStorage');

    $this->alterInfo('codes_pool_codes_storage_info');
    $this->setCacheBackend($cache_backend, 'codes_pool_codes_storage_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    foreach (['id', 'label'] as $required_property) {
      if (empty($definition[$required_property])) {
        throw new PluginException(sprintf('The codes storage "%s" must define the %s property.', $plugin_id, $required_property));
      }
    }
  }

}
