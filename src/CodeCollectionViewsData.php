<?php

namespace Drupal\codes_pool;

use Drupal\views\EntityViewsData;

/**
 * Provides views data for code collections.
 */
class CodeCollectionViewsData extends EntityViewsData {

}
