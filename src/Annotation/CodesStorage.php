<?php

namespace Drupal\codes_pool\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the codes storage plugin annotation object.
 *
 * Plugin namespace: Plugin\codes_pool\CodeStorage.
 *
 * @Annotation
 */
class CodesStorage extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
