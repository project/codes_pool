<?php

namespace Drupal\codes_pool\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the code collection source plugin annotation object.
 *
 * Plugin namespace: Plugin\codes_pool\CodeCollectionSource.
 *
 * @Annotation
 */
class CodeCollectionSource extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The collection entity type ID.
   *
   * This is the entity type ID of the entity passed to the plugin during
   * execution.
   *
   * @var string
   */
  public $entity_type;

  /**
   * Source storage.
   *
   * Available options:
   *   - local: Store data from source to local. e.g. CSV file.
   *   - remote: codes are stored in remote. e.g. API.
   *
   * @var string
   */
  public $storage;

}
