# Codes Pool



## Overview
Code collection module provides a way to upload and manage a set of codes
in Drupal system.

## Design

![Design](https://docs.google.com/drawings/d/e/2PACX-1vRM5wjdsoZm1BIS_j6xdNqspM3JkAXqsRU2jfP9PWd8qvWCZkbpBp7rXpsDJuhsOfV8q4OKEjFWSj81/pub?w=748&h=652)

### Technical Components

#### FIELD WIDGET: code collection

#### PLUGIN: code collection source

##### CSV

### API


#### PLUGIN: codes storage
Codes storage plugin provides a way to persist codes. The module comes with two
storage options:
##### Database:
Database storage takes care of creating a table per code collection entity and
CRUD operation of codes.

##### Remote:
Remote is a dummy storage plugin to connect to API endpoint and get code
and code metadata.
